#!/usr/bin/env python3

# batch-rename.py Batch rename files to files without regex

import subprocess, sys

if len(sys.argv) != 3:
	raise SystemExit("Usage: batch-rename.py {dir} {regex}")

dir = sys.argv[1]
regex = sys.argv[2]
files = subprocess.check_output('find {} -regex "{}"'.format(dir, regex), shell=True)
files = files.decode("utf-8").split("\n")
for file in files:
	if file != "":
		new_file = file.split("?")[0]
		subprocess.check_output('mv -v {} {}'.format(file, new_file), shell=True)
