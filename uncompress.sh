#!/bin/bash

# Find all gzipped files in the given directories and decompressed them
# Usage scenario: batch decompress thoese gzipped files got by wget
# Usage: ./uncompress.sh {dir}

if [ $# -ne 1 ]; then
	echo "Usage: ./uncompress.sh {dir}"
	exit 1
fi

DIR=$1
files=$(find $DIR -type file)
for file in $files; do
	file_type=$(file -b $file)
	file_type=${file_type:0:4}
	if [ $file_type == 'gzip' ]; then
		mv $file $file.gz
		gunzip -v $file.gz
	fi
done
